package br.com.cliente.Clientes.repository;

import br.com.cliente.Clientes.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente,Integer> {
}
