package br.com.cliente.Clientes.controller;


import br.com.cliente.Clientes.models.Cliente;
import br.com.cliente.Clientes.models.dto.ClienteRequest;
import br.com.cliente.Clientes.models.dto.MappingDTO;
import br.com.cliente.Clientes.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    MappingDTO mappingDTO = new MappingDTO();

    @PostMapping
    public ResponseEntity<Cliente> criarCliente(@RequestBody @Valid ClienteRequest clienteRequest){
        Cliente clienteObjeto = clienteService.criarCliente(mappingDTO.mappingCliente(clienteRequest));
        return ResponseEntity.status(201).body(clienteObjeto);
    }
    @GetMapping("/{id}")
    public ResponseEntity<Cliente> consultarClientePorId(@PathVariable(name = "id") int id){
        System.out.println("ID: " +id);
        System.out.println("Recebi uma requisição! " + System.currentTimeMillis());
        Cliente clienteObjeto = clienteService.consultarClientePorId(id);
        return ResponseEntity.status(200).body(clienteObjeto);
    }


}
