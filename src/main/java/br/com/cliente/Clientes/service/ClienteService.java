package br.com.cliente.Clientes.service;


import br.com.cliente.Clientes.exception.ClienteNotFoundException;
import br.com.cliente.Clientes.models.Cliente;
import br.com.cliente.Clientes.repository.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Cliente criarCliente(Cliente cliente){
        return clienteRepository.save(cliente);
    }

    public Cliente consultarClientePorId(int id){
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);
        if(optionalCliente.isPresent())
            return optionalCliente.get();
        throw new ClienteNotFoundException();
    }
}
