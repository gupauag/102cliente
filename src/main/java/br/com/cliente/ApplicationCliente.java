package br.com.cliente;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApplicationCliente {

	public static void main(String[] args) {
		SpringApplication.run(ApplicationCliente.class, args);
	}

}
